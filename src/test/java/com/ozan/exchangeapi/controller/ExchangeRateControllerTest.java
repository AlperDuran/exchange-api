package com.ozan.exchangeapi.controller;

import com.ozan.exchangeapi.model.enums.Symbol;
import com.ozan.exchangeapi.service.ExchangeRateService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ExchangeRateController.class)
public class ExchangeRateControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ExchangeRateService exchangeRateService;

    @Test
    public void it_should_get_exchange_rate() throws Exception {
        //given
        ArgumentCaptor<Symbol> argumentCaptor0 = ArgumentCaptor.forClass(Symbol.class);
        ArgumentCaptor<Symbol> argumentCaptor1 = ArgumentCaptor.forClass(Symbol.class);
        given(exchangeRateService.getExchangeRate(argumentCaptor0.capture(), argumentCaptor1.capture())).willReturn(10.3D);

        //when
        ResultActions resultActions = mockMvc.perform(get("/exchange-rate/USD?base=TRY"));

        //then
        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(10.3D)));
        Symbol value0 = argumentCaptor0.getValue();
        Symbol value1 = argumentCaptor1.getValue();
        assertThat(value0).isEqualTo(Symbol.USD);
        assertThat(value1).isEqualTo(Symbol.TRY);
    }
}