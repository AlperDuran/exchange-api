package com.ozan.exchangeapi.controller.advice;

import com.ozan.exchangeapi.exception.ConversionException;
import com.ozan.exchangeapi.exception.ExchangeRateException;
import com.ozan.exchangeapi.exception.RatesApiException;
import feign.FeignException;
import feign.Request;
import feign.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = TestController.class)
public class GlobalControllerExceptionHandlerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void it_should_respond_with_400_for_ExchangeRateException() throws Exception {
        //given

        //when
        ResultActions resultActions = mockMvc.perform(get("/exchange-rate-exception"));

        //then
        resultActions
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.exception", is("ExchangeRateException")))
                .andExpect(jsonPath("$.timestamp", notNullValue()));
    }

    @Test
    public void it_should_respond_with_400_for_RatesApiException() throws Exception {
        //given

        //when
        ResultActions resultActions = mockMvc.perform(get("/rates-api-exception"));

        //then
        resultActions
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.exception", is("RatesApiException")))
                .andExpect(jsonPath("$.timestamp", notNullValue()));
    }

    @Test
    public void it_should_respond_with_400_for_ConversionException() throws Exception {
        //given

        //when
        ResultActions resultActions = mockMvc.perform(get("/conversion-exception"));

        //then
        resultActions
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.exception", is("ConversionException")))
                .andExpect(jsonPath("$.timestamp", notNullValue()));
    }


    @Test
    public void it_should_respond_with_resolved_feign_exception_with_its_body() throws Exception {
        //given
        //when
        ResultActions resultActions = mockMvc.perform(get("/feign-exception"));
        //then
        resultActions
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.exception", is("FeignException")))
                .andExpect(jsonPath("$.errors[0]", is("error-message")))
                .andExpect(jsonPath("$.timestamp", is(11111)));
    }

    @Test
    public void it_should_respond_with_resolved_feign_exception_with_non_json_body_if_not_containss() throws Exception {
        //given
        //when
        ResultActions resultActions = mockMvc.perform(get("/feign-exception-no-json-response-body"));
        //then
        resultActions
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.exception", is("ApiClientException")))
                .andExpect(jsonPath("$.errors[0]", is("Api client exception occurred. Detail: Error Occurred")))
                .andExpect(jsonPath("$.timestamp", notNullValue()));
    }
}

@RestController
class TestController {

    @GetMapping("/rates-api-exception")
    public void RatesApiException() {
        throw new RatesApiException("message");
    }

    @GetMapping("/exchange-rate-exception")
    public void ExchangeRateException() {
        throw new ExchangeRateException();
    }

    @GetMapping("/conversion-exception")
    public void ConversionException() {
        throw new ConversionException();
    }

    @GetMapping("/feign-exception")
    public void throwFeignException() {
        String responseBody = "{\"exception\": \"FeignException\", \"timestamp\": 11111, \"errors\": [\"error-message\"]}";
        Request request = Request.create(Request.HttpMethod.GET, "http://www.hurriyetemlak.com", new HashMap<>(), new byte[]{1, 2, 3}, StandardCharsets.UTF_8);
        Response response = Response.builder().request(request).status(400).body(responseBody, StandardCharsets.UTF_8).headers(new HashMap<>()).build();
        throw FeignException.errorStatus("methodKey", response);
    }

    @GetMapping("/feign-exception-no-json-response-body")
    public void throwFeignExceptionWithNoJsonResponseBody() {
        String responseBody = "Error Occurred";
        Request request = Request.create(Request.HttpMethod.GET, "http://www.hurriyetemlak.com", new HashMap<>(), new byte[]{1, 2, 3}, StandardCharsets.UTF_8);
        Response response = Response.builder().request(request).status(400).body(responseBody, StandardCharsets.UTF_8).headers(new HashMap<>()).build();
        throw FeignException.errorStatus("methodKey", response);
    }
}