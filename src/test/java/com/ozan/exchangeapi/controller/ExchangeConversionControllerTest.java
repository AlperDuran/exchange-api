package com.ozan.exchangeapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ozan.exchangeapi.model.enums.Symbol;
import com.ozan.exchangeapi.model.request.ExchangeConversionFilterRequest;
import com.ozan.exchangeapi.model.request.ExchangeConversionRequest;
import com.ozan.exchangeapi.model.response.ExchangeConversionResponse;
import com.ozan.exchangeapi.model.response.PageableResult;
import com.ozan.exchangeapi.service.ExchangeConversionService;
import com.ozan.exchangeapi.utils.Clock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Collections;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ExchangeConversionController.class)
public class ExchangeConversionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ExchangeConversionService exchangeConversionService;

    @Test
    public void it_should_convert() throws Exception {
        //given
        ExchangeConversionRequest exchangeConversionRequest = ExchangeConversionRequest.builder()
                .targetCurrency(Symbol.USD)
                .sourceCurrency(Symbol.TRY)
                .sourceAmount(10.3D)
                .build();

        ExchangeConversionResponse exchangeConversionResponse = ExchangeConversionResponse.builder()
                .transactionId(0)
                .currency(Symbol.USD)
                .amount(20.3D)
                .build();

        ArgumentCaptor<ExchangeConversionRequest> argumentCaptor = ArgumentCaptor.forClass(ExchangeConversionRequest.class);

        given(exchangeConversionService.convert(argumentCaptor.capture())).willReturn(exchangeConversionResponse);

        //when
        ResultActions resultActions = mockMvc.perform(post("/conversion")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(exchangeConversionRequest)));

        //then
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.transactionId", is(0)))
                .andExpect(jsonPath("$.currency", is(Symbol.USD.name())))
                .andExpect(jsonPath("$.amount", is(20.3D)));
    }

    @Test
    public void it_should_get_exchange_conversion_filter() throws Exception {
        //given
        Clock.freeze();

        ExchangeConversionResponse exchangeExchangeConversionResponse = ExchangeConversionResponse.builder()
                .currency(Symbol.USD)
                .amount(10.3D)
                .transactionId(0)
                .build();

        PageableResult<ExchangeConversionResponse> pageableResult = new PageableResult<>();
        pageableResult.setContent(Collections.singletonList(exchangeExchangeConversionResponse));

        ArgumentCaptor<ExchangeConversionFilterRequest> argumentCaptor = ArgumentCaptor.forClass(ExchangeConversionFilterRequest.class);
        given(exchangeConversionService.getExchangeConversionFilter(argumentCaptor.capture())).willReturn(pageableResult);

        //when
        ResultActions resultActions = mockMvc.perform(get("/conversion")
                .param("transactionId", "0")
        );

        //then
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].currency", is(Symbol.USD.name())))
                .andExpect(jsonPath("$.content[0].transactionId", is(0)))
                .andExpect(jsonPath("$.content[0].amount", is(10.3D)));
        ExchangeConversionFilterRequest value = argumentCaptor.getValue();
        assertThat(value.getTransactionId()).isZero();
    }
}