package com.ozan.exchangeapi.repository.specification;

import com.ozan.exchangeapi.client.RatesApiClient;
import com.ozan.exchangeapi.domain.ExchangeConversion;
import com.ozan.exchangeapi.model.enums.Symbol;
import com.ozan.exchangeapi.model.request.ExchangeConversionFilterRequest;
import com.ozan.exchangeapi.repository.ExchangeConversionRepository;
import com.ozan.exchangeapi.utils.Clock;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

@DataJpaTest
@RunWith(SpringRunner.class)
public class ExchangeConversionSpecificationTest {

    @Autowired
    private ExchangeConversionRepository exchangeConversionRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @MockBean
    private RatesApiClient ratesApiClient;

    @Test
    public void it_should_find_exchange_conversion_with_transaction_id() {
        //given
        ExchangeConversion exchangeConversion0 = ExchangeConversion.builder()
                .targetAmount(30.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(40.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(5.0D)
                .build();

        ExchangeConversion exchangeConversion1 = ExchangeConversion.builder()
                .targetAmount(60.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(70.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(8.0D)
                .build();

        ExchangeConversion exchangeConversion2 = ExchangeConversion.builder()
                .targetAmount(90.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(100.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(11.0D)
                .build();

        ExchangeConversion exchangeConversion3 = ExchangeConversion.builder()
                .targetAmount(120.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(130.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(14.0D)
                .build();

        testEntityManager.persistAndFlush(exchangeConversion0);
        testEntityManager.persistAndFlush(exchangeConversion1);
        testEntityManager.persistAndFlush(exchangeConversion2);
        testEntityManager.persistAndFlush(exchangeConversion3);


        ExchangeConversionFilterRequest exchangeConversionFilterRequest = ExchangeConversionFilterRequest.builder()
                .transactionId(exchangeConversion1.getId())
                .build();

        PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "createdDate"));

        //when
        Page<ExchangeConversion> result = exchangeConversionRepository.findAll(ExchangeConversionSpecification.getFilterQuery(exchangeConversionFilterRequest), pageable);

        //then
        Assertions.assertThat(result.getContent().size()).isEqualTo(1);
        Assertions.assertThat(result.getContent().get(0)).isEqualTo(exchangeConversion1);
    }

    @Test
    public void it_should_find_exchange_conversion_with_min_date() {
        //given
        Clock.freeze();
        ExchangeConversion exchangeConversion0 = ExchangeConversion.builder()
                .targetAmount(30.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(40.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(5.0D)
                .createdDate(Clock.now().plusDays(1))
                .build();

        ExchangeConversion exchangeConversion1 = ExchangeConversion.builder()
                .targetAmount(60.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(70.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(8.0D)
                .createdDate(Clock.now().plusDays(2))
                .build();

        ExchangeConversion exchangeConversion2 = ExchangeConversion.builder()
                .targetAmount(90.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(100.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(11.0D)
                .createdDate(Clock.now().plusDays(3))
                .build();

        ExchangeConversion exchangeConversion3 = ExchangeConversion.builder()
                .targetAmount(120.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(130.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(14.0D)
                .createdDate(Clock.now().plusDays(4))
                .build();

        testEntityManager.persistAndFlush(exchangeConversion0);
        testEntityManager.persistAndFlush(exchangeConversion1);
        testEntityManager.persistAndFlush(exchangeConversion2);
        testEntityManager.persistAndFlush(exchangeConversion3);


        ExchangeConversionFilterRequest exchangeConversionFilterRequest = ExchangeConversionFilterRequest.builder()
                .minDate(Clock.now().plusDays(2))
                .build();

        PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "createdDate"));

        //when
        Page<ExchangeConversion> result = exchangeConversionRepository.findAll(ExchangeConversionSpecification.getFilterQuery(exchangeConversionFilterRequest), pageable);

        //then
        Assertions.assertThat(result.getContent().size()).isEqualTo(3);
    }

    @Test
    public void it_should_find_exchange_conversion_with_max_date() {
        //given
        Clock.freeze();
        ExchangeConversion exchangeConversion0 = ExchangeConversion.builder()
                .targetAmount(30.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(40.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(5.0D)
                .createdDate(Clock.now().plusDays(1))
                .build();

        ExchangeConversion exchangeConversion1 = ExchangeConversion.builder()
                .targetAmount(60.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(70.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(8.0D)
                .createdDate(Clock.now().plusDays(2))
                .build();

        ExchangeConversion exchangeConversion2 = ExchangeConversion.builder()
                .targetAmount(90.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(100.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(11.0D)
                .createdDate(Clock.now().plusDays(3))
                .build();

        ExchangeConversion exchangeConversion3 = ExchangeConversion.builder()
                .targetAmount(120.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(130.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(14.0D)
                .createdDate(Clock.now().plusDays(4))
                .build();

        testEntityManager.persistAndFlush(exchangeConversion0);
        testEntityManager.persistAndFlush(exchangeConversion1);
        testEntityManager.persistAndFlush(exchangeConversion2);
        testEntityManager.persistAndFlush(exchangeConversion3);


        ExchangeConversionFilterRequest exchangeConversionFilterRequest = ExchangeConversionFilterRequest.builder()
                .maxDate(Clock.now().plusDays(2))
                .build();

        PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "createdDate"));

        //when
        Page<ExchangeConversion> result = exchangeConversionRepository.findAll(ExchangeConversionSpecification.getFilterQuery(exchangeConversionFilterRequest), pageable);

        //then
        Assertions.assertThat(result.getContent().size()).isEqualTo(2);
    }

    @Test
    public void it_should_find_exchange_conversion_with_max_date_and_min_date() {
        //given
        Clock.freeze();
        ExchangeConversion exchangeConversion0 = ExchangeConversion.builder()
                .targetAmount(30.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(40.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(5.0D)
                .createdDate(Clock.now().plusDays(1))
                .build();

        ExchangeConversion exchangeConversion1 = ExchangeConversion.builder()
                .targetAmount(60.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(70.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(8.0D)
                .createdDate(Clock.now().plusDays(2))
                .build();

        ExchangeConversion exchangeConversion2 = ExchangeConversion.builder()
                .targetAmount(90.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(100.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(11.0D)
                .createdDate(Clock.now().plusDays(3))
                .build();

        ExchangeConversion exchangeConversion3 = ExchangeConversion.builder()
                .targetAmount(120.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(130.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(14.0D)
                .createdDate(Clock.now().plusDays(4))
                .build();

        testEntityManager.persistAndFlush(exchangeConversion0);
        testEntityManager.persistAndFlush(exchangeConversion1);
        testEntityManager.persistAndFlush(exchangeConversion2);
        testEntityManager.persistAndFlush(exchangeConversion3);


        ExchangeConversionFilterRequest exchangeConversionFilterRequest = ExchangeConversionFilterRequest.builder()
                .minDate(Clock.now().plusDays(2))
                .maxDate(Clock.now().plusDays(4))
                .build();

        PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "createdDate"));

        //when
        Page<ExchangeConversion> result = exchangeConversionRepository.findAll(ExchangeConversionSpecification.getFilterQuery(exchangeConversionFilterRequest), pageable);

        //then
        Assertions.assertThat(result.getContent().size()).isEqualTo(3);
    }
}