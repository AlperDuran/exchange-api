package com.ozan.exchangeapi.converter;

import com.ozan.exchangeapi.model.enums.Symbol;
import com.ozan.exchangeapi.model.response.ExchangeRateResponse;
import com.ozan.exchangeapi.utils.Clock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeRateConverterTest {

    @InjectMocks
    private ExchangeRateConverter exchangeRateConverter;

    @Test
    public void it_should_return_exchange_rate() {
        //given
        Clock.freeze();
        Map<Symbol, Double> map = new HashMap<>();
        map.put(Symbol.USD, 10.3D);
        ExchangeRateResponse exchangeRateResponse = ExchangeRateResponse.builder()
                .base(Symbol.TRY)
                .rates(map)
                .date(Clock.now().toString())
                .build();
        //when
        Optional<Double> result = exchangeRateConverter.apply(exchangeRateResponse, Symbol.USD);

        //then
        assertThat(result).isEqualTo(Optional.of(10.3D));
    }

    @Test
    public void it_should_return_empty() {
        //given
        Clock.freeze();
        Map<Symbol, Double> map = new HashMap<>();
        map.put(Symbol.USD, 10.3D);
        ExchangeRateResponse exchangeRateResponse = ExchangeRateResponse.builder()
                .base(Symbol.TRY)
                .rates(map)
                .date(Clock.now().toString())
                .build();
        //when
        Optional<Double> result = exchangeRateConverter.apply(exchangeRateResponse, Symbol.TRY);

        //then
        assertThat(result).isEmpty();
    }
}