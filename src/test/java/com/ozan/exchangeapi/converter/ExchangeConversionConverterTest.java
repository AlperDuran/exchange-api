package com.ozan.exchangeapi.converter;

import com.ozan.exchangeapi.domain.ExchangeConversion;
import com.ozan.exchangeapi.model.enums.Symbol;
import com.ozan.exchangeapi.model.response.ExchangeConversionResponse;
import com.ozan.exchangeapi.model.response.PageableResult;
import org.assertj.core.groups.Tuple;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeConversionConverterTest {

    @InjectMocks
    private ExchangeConversionConverter exchangeConversionConverter;

    @Test
    public void it_should_convert_to_exchangeConversion() {
        //given

        //when
        ExchangeConversion result = exchangeConversionConverter.apply(10.0D, 2.0D, Symbol.USD, Symbol.TRY);

        //then
        assertThat(result.getTargetAmount()).isEqualTo(20.0D);
        assertThat(result.getTargetCurrency()).isEqualTo(Symbol.USD.name());
        assertThat(result.getSourceAmount()).isEqualTo(10.0D);
        assertThat(result.getSourceCurrency()).isEqualTo(Symbol.TRY.name());
        assertThat(result.getExchangeRate()).isEqualTo(2.0D);
    }

    @Test
    public void it_should_convert_to_exchangeConversion_response() {
        //given
        ExchangeConversion exchangeConversion = ExchangeConversion.builder()
                .id(0)
                .targetAmount(20.0D)
                .targetCurrency(Symbol.USD.name())
                .sourceAmount(10.0D)
                .sourceCurrency(Symbol.TRY.name())
                .exchangeRate(2.0D)
                .build();

        //when
        ExchangeConversionResponse result = exchangeConversionConverter.apply(exchangeConversion);

        //then
        assertThat(result.getAmount()).isEqualTo(20.0D);
        assertThat(result.getCurrency()).isEqualTo(Symbol.USD);
        assertThat(result.getTransactionId()).isZero();
    }

    @Test
    public void it_should_convert_page() {
        //given
        ExchangeConversion exchangeConversion = ExchangeConversion.builder()
                .id(0)
                .exchangeRate(1.0D)
                .sourceAmount(2.0D)
                .targetAmount(3.0D)
                .sourceCurrency(Symbol.TRY.name())
                .targetCurrency(Symbol.GBP.name())
                .build();
        final PageRequest pageable = PageRequest.of(0, 20);
        final long totalElements = 1L;
        Page<ExchangeConversion> exchangeConversionPage = new PageImpl<>(Collections.singletonList(exchangeConversion), pageable, totalElements);

        //when
        PageableResult<ExchangeConversionResponse> result = exchangeConversionConverter.applyPage(exchangeConversionPage);

        //then
        assertThat(result.getContent()).extracting("amount", "currency", "transactionId").containsExactly(Tuple.tuple(3.0D, Symbol.GBP, 0));
    }
}