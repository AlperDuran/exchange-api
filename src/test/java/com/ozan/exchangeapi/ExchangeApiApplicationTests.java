package com.ozan.exchangeapi;

import com.ozan.exchangeapi.client.RatesApiClient;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class ExchangeApiApplicationTests {

    @MockBean
    private RatesApiClient ratesApiClient;

    @Test
    void contextLoads() {
    }

}
