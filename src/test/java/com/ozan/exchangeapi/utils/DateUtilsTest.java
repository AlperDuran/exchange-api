package com.ozan.exchangeapi.utils;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class DateUtilsTest {

    @Test
    public void it_should_get_year_difference_zero() {
        //Given
        LocalDateTime date1 = LocalDateTime.of(2069, 3, 1, 3, 1, 31);
        LocalDateTime date2 = LocalDateTime.of(2070, 3, 1, 3, 1, 30);
        Clock.freeze(date2);

        //When
        final Integer yearDifference = DateUtils.getYearDifference(date1);

        //Then
        assertThat(yearDifference).isZero();
        Clock.unfreeze();
    }

    @Test
    public void it_should_get_year_difference() {
        //Given
        LocalDateTime date1 = LocalDateTime.of(2069, 3, 1, 3, 1, 31);
        LocalDateTime date2 = LocalDateTime.of(2070, 3, 1, 3, 1, 32);
        Clock.freeze(date2);

        //When
        final Integer yearDifference = DateUtils.getYearDifference(date1);

        //Then
        assertThat(yearDifference).isEqualTo(1);
        Clock.unfreeze();
    }

    @Test
    public void it_should_get_day_difference() {
        //Given
        LocalDateTime date1 = LocalDateTime.of(2070, 3, 2, 3, 1, 31);
        LocalDateTime date2 = LocalDateTime.of(2070, 3, 1, 3, 1, 30);
        Clock.freeze(date2);

        //When
        final Integer yearDifference = DateUtils.calculateRemainingDays(date1);

        //Then
        assertThat(yearDifference).isEqualTo(1);
        Clock.unfreeze();
    }

    @Test
    public void it_should_get_year_difference_same_year() {
        //Given
        LocalDateTime date1 = LocalDateTime.of(2069, 3, 1, 3, 1, 31);
        Clock.freeze(date1);

        //When
        final Integer yearDifference = DateUtils.getYearDifference(date1);

        //Then
        assertThat(yearDifference).isZero();
        Clock.unfreeze();
    }

    @Test
    public void it_should_get_year_difference_1_year_later_exactly() {
        //Given
        LocalDateTime date1 = LocalDateTime.of(2069, 3, 1, 3, 1, 31);
        LocalDateTime date2 = LocalDateTime.of(2070, 3, 1, 3, 1, 31);
        Clock.freeze(date2);

        //When
        final Integer yearDifference = DateUtils.getYearDifference(date1);

        //Then
        assertThat(yearDifference).isEqualTo(1);
        Clock.unfreeze();
    }

    @Test
    public void it_should_convert_local_date_time_to_date() {
        //Given
        LocalDateTime ldt = LocalDateTime.of(2019, 3, 1, 3, 1, 31);
        ZonedDateTime zdt = ldt.atZone(ZoneId.systemDefault());

        //When
        final Date date = DateUtils.convertLocalDateTimeToDate(ldt);

        //Then
        assertThat(date.getTime()).isEqualTo(zdt.toInstant().toEpochMilli());
    }

    @Test
    public void it_should_convert_date_to_local_date_time() {
        // Given
        Date date = new Date();

        // When
        LocalDateTime localDateTime = DateUtils.convertDateToLocalDateTime(date);

        // Then
        ZonedDateTime zdt = localDateTime.atZone(ZoneId.systemDefault());
        assertThat(zdt.toInstant().toEpochMilli()).isEqualTo(date.getTime());
    }

    @Test
    public void it_should_get_minute_difference() {
        //Given
        LocalDateTime date1 = LocalDateTime.of(2021, 3, 1, 3, 1, 30);
        LocalDateTime date2 = LocalDateTime.of(2021, 3, 1, 3, 2, 30);
        Clock.freeze(date2);

        //When
        final Integer minuteDifference = DateUtils.getMinuteDifference(date1);

        //Then
        assertThat(minuteDifference).isOne();
        Clock.unfreeze();
    }
}