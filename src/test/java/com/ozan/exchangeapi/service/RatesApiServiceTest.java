package com.ozan.exchangeapi.service;

import com.ozan.exchangeapi.client.RatesApiClient;
import com.ozan.exchangeapi.model.enums.Symbol;
import com.ozan.exchangeapi.model.response.ExchangeRateResponse;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.rule.OutputCapture;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class RatesApiServiceTest {

    @InjectMocks
    private RatesApiService ratesApiService;

    @Mock
    private RatesApiClient ratesApiClient;

    @Rule
    public final OutputCapture outputCapture = new OutputCapture();

    @Test
    public void it_should_get_exchange() {
        //given
        ExchangeRateResponse exchangeRateResponse = ExchangeRateResponse.builder().build();

        given(ratesApiClient.getExchangeRate(Symbol.USD, Symbol.TRY)).willReturn(exchangeRateResponse);

        //when
        ExchangeRateResponse result = ratesApiService.getExchangeRate(Symbol.USD, Symbol.TRY);

        //then
        assertThat(result).isEqualTo(exchangeRateResponse);
    }

    @Test
    public void it_should_log_cache() {
        //given

        //when
        ratesApiService.evictExchangeCacheValues();

        //then
        outputCapture.expect(containsString("Evict exchanges cache values"));

    }
}