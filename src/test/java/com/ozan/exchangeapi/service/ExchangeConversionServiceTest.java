package com.ozan.exchangeapi.service;

import com.ozan.exchangeapi.converter.ExchangeConversionConverter;
import com.ozan.exchangeapi.converter.ExchangeRateConverter;
import com.ozan.exchangeapi.domain.ExchangeConversion;
import com.ozan.exchangeapi.exception.ConversionException;
import com.ozan.exchangeapi.model.enums.Symbol;
import com.ozan.exchangeapi.model.request.ExchangeConversionFilterRequest;
import com.ozan.exchangeapi.model.request.ExchangeConversionRequest;
import com.ozan.exchangeapi.model.response.ExchangeConversionResponse;
import com.ozan.exchangeapi.model.response.ExchangeRateResponse;
import com.ozan.exchangeapi.model.response.PageableResult;
import com.ozan.exchangeapi.repository.ExchangeConversionRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verifyNoInteractions;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeConversionServiceTest {

    @InjectMocks
    private ExchangeConversionService exchangeConversionService;

    @Mock
    private RatesApiService ratesApiService;

    @Mock
    private ExchangeRateConverter exchangeRateConverter;

    @Mock
    private ExchangeConversionConverter exchangeConversionConverter;

    @Mock
    private ExchangeConversionRepository exchangeConversionRepository;

    @Test
    public void it_should_convert_and_save_exchange() {
        //given
        ExchangeConversionRequest exchangeConversionRequest = ExchangeConversionRequest.builder()
                .targetCurrency(Symbol.USD)
                .sourceCurrency(Symbol.TRY)
                .sourceAmount(10.3D)
                .build();

        ExchangeConversionResponse exchangeConversionResponse = ExchangeConversionResponse.builder()
                .transactionId(0)
                .currency(Symbol.USD)
                .amount(10.3D)
                .build();

        ExchangeConversion exchangeConversion = ExchangeConversion.builder().build();

        ExchangeRateResponse exchangeRateResponse = ExchangeRateResponse.builder().build();


        given(ratesApiService.getExchangeRate(exchangeConversionRequest.getTargetCurrency(), exchangeConversionRequest.getSourceCurrency())).willReturn(exchangeRateResponse);
        given(exchangeRateConverter.apply(exchangeRateResponse, exchangeConversionRequest.getTargetCurrency())).willReturn(Optional.of(10.3D));
        given(exchangeConversionConverter.apply(10.3D, 10.3D, Symbol.USD, Symbol.TRY)).willReturn(exchangeConversion);
        given(exchangeConversionRepository.save(exchangeConversion)).willReturn(exchangeConversion);
        given(exchangeConversionConverter.apply(exchangeConversion)).willReturn(exchangeConversionResponse);

        //when
        ExchangeConversionResponse result = exchangeConversionService.convert(exchangeConversionRequest);

        //then
        assertThat(result).isEqualTo(exchangeConversionResponse);
    }

    @Test
    public void it_should_throw_exception() {
        //given
        ExchangeConversionRequest exchangeConversionRequest = ExchangeConversionRequest.builder()
                .targetCurrency(Symbol.USD)
                .sourceCurrency(Symbol.TRY)
                .sourceAmount(10.3D)
                .build();

        ExchangeRateResponse exchangeRateResponse = ExchangeRateResponse.builder().build();


        given(ratesApiService.getExchangeRate(exchangeConversionRequest.getTargetCurrency(), exchangeConversionRequest.getSourceCurrency())).willReturn(exchangeRateResponse);
        given(exchangeRateConverter.apply(exchangeRateResponse, exchangeConversionRequest.getTargetCurrency())).willReturn(Optional.empty());

        //when
        ConversionException thrown = (ConversionException) catchThrowable(() -> exchangeConversionService.convert(exchangeConversionRequest));

        //then
        assertThat(thrown).isInstanceOf(ConversionException.class);
        verifyNoInteractions(exchangeConversionConverter, exchangeConversionRepository);
    }

    @Test
    public void it_should_get_exchange_conversion_list() {
        //Given
        Page<ExchangeConversion> exchangeConversionPage = new PageImpl<>(Collections.singletonList(ExchangeConversion.builder().build()));
        given(exchangeConversionRepository.findAll(any(Specification.class), any(Pageable.class))).willReturn(exchangeConversionPage);

        ExchangeConversionResponse exchangeConversionDto = ExchangeConversionResponse.builder().build();
        PageableResult<ExchangeConversionResponse> pageableResult = new PageableResult<>();
        pageableResult.setContent(Collections.singletonList(exchangeConversionDto));
        given(exchangeConversionConverter.applyPage(exchangeConversionPage)).willReturn(pageableResult);

        ExchangeConversionFilterRequest exchangeConversionFilterRequest = ExchangeConversionFilterRequest.builder().transactionId(10).build();
        exchangeConversionFilterRequest.setPage(1);

        //When
        PageableResult<ExchangeConversionResponse> actualExchangeConversionResponses = exchangeConversionService.getExchangeConversionFilter(exchangeConversionFilterRequest);

        //Then
        assertThat(actualExchangeConversionResponses.getContent()).containsExactly(exchangeConversionDto);
    }
}