package com.ozan.exchangeapi.service;

import com.ozan.exchangeapi.converter.ExchangeRateConverter;
import com.ozan.exchangeapi.exception.ExchangeRateException;
import com.ozan.exchangeapi.model.enums.Symbol;
import com.ozan.exchangeapi.model.response.ExchangeRateResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeRateServiceTest {

    @InjectMocks
    private ExchangeRateService exchangeRateService;

    @Mock
    private ExchangeRateConverter exchangeRateConverter;

    @Mock
    private RatesApiService ratesApiService;

    @Test
    public void it_should_get_exchange_rate() {
        //given
        ExchangeRateResponse exchangeRateResponse = ExchangeRateResponse.builder().build();

        given(ratesApiService.getExchangeRate(Symbol.USD, Symbol.TRY)).willReturn(exchangeRateResponse);
        given(exchangeRateConverter.apply(exchangeRateResponse, Symbol.USD)).willReturn(Optional.of(10.3D));

        //when
        Double result = exchangeRateService.getExchangeRate(Symbol.USD, Symbol.TRY);

        //then
        assertThat(result).isEqualTo(10.3D);
    }

    @Test
    public void it_should_throw_exception_when_rate_not_found() {
        //given
        ExchangeRateResponse exchangeRateResponse = ExchangeRateResponse.builder().build();

        given(ratesApiService.getExchangeRate(Symbol.USD, Symbol.TRY)).willReturn(exchangeRateResponse);
        given(exchangeRateConverter.apply(exchangeRateResponse, Symbol.USD)).willReturn(Optional.empty());

        //when
        ExchangeRateException thrown = (ExchangeRateException) catchThrowable(() -> exchangeRateService.getExchangeRate(Symbol.USD, Symbol.TRY));

        //then
        assertThat(thrown).isInstanceOf(ExchangeRateException.class);
    }
}