package com.ozan.exchangeapi.controller.advice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ozan.exchangeapi.exception.ConversionException;
import com.ozan.exchangeapi.exception.ExchangeRateException;
import com.ozan.exchangeapi.exception.RatesApiException;
import com.ozan.exchangeapi.model.response.ErrorResponse;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;
import java.util.Collections;

@RestControllerAdvice
@Slf4j
@RequiredArgsConstructor
public class GlobalControllerExceptionHandler {

    private final ObjectMapper objectMapper;

    @ExceptionHandler(ExchangeRateException.class)
    public ResponseEntity<ErrorResponse> handle(ExchangeRateException exception) {
        log.error("Exchange Rate Exception occurred.", exception);
        ErrorResponse errorResponse = new ErrorResponse(
                "ExchangeRateException",
                System.currentTimeMillis(),
                Collections.singletonList(exception.getMessage())
        );
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RatesApiException.class)
    public ResponseEntity<ErrorResponse> handle(RatesApiException exception) {
        log.error("Rates Api Exception occurred.", exception);
        ErrorResponse errorResponse = new ErrorResponse(
                "RatesApiException",
                System.currentTimeMillis(),
                Collections.singletonList(exception.getMessage())
        );
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConversionException.class)
    public ResponseEntity<ErrorResponse> handle(ConversionException exception) {
        log.error("Conversion Exception occurred.", exception);
        ErrorResponse errorResponse = new ErrorResponse(
                "ConversionException",
                System.currentTimeMillis(),
                Collections.singletonList(exception.getMessage())
        );
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FeignException.class)
    public ResponseEntity<ErrorResponse> handleException(FeignException exception) {
        log.error("Api client exception occurred. Detail: " + exception.contentUTF8(), exception);
        return new ResponseEntity<>(resolve(exception), HttpStatus.valueOf(exception.status()));
    }

    private ErrorResponse resolve(FeignException exception) {
        try {
            return objectMapper.readValue(exception.contentUTF8(), ErrorResponse.class);
        } catch (IOException e) {
            return new ErrorResponse(
                    "ApiClientException",
                    System.currentTimeMillis(),
                    Collections.singletonList("Api client exception occurred. Detail: " + exception.contentUTF8())
            );
        }
    }
}
