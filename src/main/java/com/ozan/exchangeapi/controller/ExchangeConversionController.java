package com.ozan.exchangeapi.controller;

import com.ozan.exchangeapi.model.request.ExchangeConversionFilterRequest;
import com.ozan.exchangeapi.model.request.ExchangeConversionRequest;
import com.ozan.exchangeapi.model.response.ExchangeConversionResponse;
import com.ozan.exchangeapi.model.response.PageableResult;
import com.ozan.exchangeapi.service.ExchangeConversionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("conversion")
public class ExchangeConversionController {

    private final ExchangeConversionService exchangeConversionService;

    @PostMapping
    public ExchangeConversionResponse convert(@RequestBody ExchangeConversionRequest exchangeConversionRequest) {
        return exchangeConversionService.convert(exchangeConversionRequest);
    }

    @GetMapping
    public PageableResult<ExchangeConversionResponse> getExchangeConversionFilter(ExchangeConversionFilterRequest conversionFilterRequest) {
        return exchangeConversionService.getExchangeConversionFilter(conversionFilterRequest);
    }
}
