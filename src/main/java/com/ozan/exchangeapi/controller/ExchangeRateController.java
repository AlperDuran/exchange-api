package com.ozan.exchangeapi.controller;

import com.ozan.exchangeapi.model.enums.Symbol;
import com.ozan.exchangeapi.service.ExchangeRateService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("exchange-rate")
public class ExchangeRateController {

    private final ExchangeRateService exchangeRateService;

    @GetMapping("{symbol}")
    public Double getExchangeRate(@PathVariable("symbol") Symbol symbol,
                                  @RequestParam("base") Symbol base) {
        return exchangeRateService.getExchangeRate(symbol, base);
    }
}
