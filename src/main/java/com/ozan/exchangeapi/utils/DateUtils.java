package com.ozan.exchangeapi.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateUtils {

    public static Integer getYearDifference(LocalDateTime date) {
        return Math.toIntExact(date.until(Clock.now(), ChronoUnit.YEARS));
    }

    public static Integer getMinuteDifference(LocalDateTime date) {
        return Math.toIntExact(date.until(Clock.now(), ChronoUnit.MINUTES));
    }

    public static LocalDateTime convertDateToLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }


    public static Integer calculateRemainingDays(LocalDateTime date) {
        return Math.toIntExact(Clock.now().until(date, ChronoUnit.DAYS));
    }

    public static Date convertLocalDateTimeToDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
