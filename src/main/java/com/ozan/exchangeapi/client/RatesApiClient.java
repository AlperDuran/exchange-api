package com.ozan.exchangeapi.client;

import com.ozan.exchangeapi.configuration.feign.RatesApiClientFeignConfiguration;
import com.ozan.exchangeapi.model.enums.Symbol;
import com.ozan.exchangeapi.model.response.ExchangeRateResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
        value = "ratesApiClient",
        url = "${rates-api.url}",
        configuration = RatesApiClientFeignConfiguration.class
)
public interface RatesApiClient {

    @GetMapping(value = "/latest")
    ExchangeRateResponse getExchangeRate(@RequestParam("symbols") Symbol target,
                                         @RequestParam("base") Symbol source);
}
