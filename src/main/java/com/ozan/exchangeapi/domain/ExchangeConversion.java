package com.ozan.exchangeapi.domain;

import com.ozan.exchangeapi.model.enums.Symbol;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "conversion", schema = "conversion_schema")
public class ExchangeConversion {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "target_amount")
    private Double targetAmount;

    @Column(name = "target_currency")
    private String targetCurrency;

    @Column(name = "source_amount")
    private Double sourceAmount;

    @Column(name = "source_currency")
    private String sourceCurrency;

    @Column(name = "exchange_rate")
    private Double exchangeRate;

    @Column(name = "created_date")
    private LocalDateTime createdDate;

    @Column(name = "updated_date")
    private LocalDateTime updatedDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ExchangeConversion exchangeConversion = (ExchangeConversion) o;
        return Objects.equals(id, exchangeConversion.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
