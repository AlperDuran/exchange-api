package com.ozan.exchangeapi.configuration.feign;

import feign.Contract;
import feign.Request;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.context.annotation.Bean;

public class RatesApiClientFeignConfiguration {

    @Bean
    public RatesApiClientErrorDecoder ratesApiClientErrorDecoder() {
        return new RatesApiClientErrorDecoder();
    }

    @Bean
    public Request.Options options() {
        return new Request.Options();
    }

    @Bean
    public Contract useRatesApiClientFeignAnnotations() {
        return new SpringMvcContract();
    }
}
