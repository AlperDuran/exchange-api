package com.ozan.exchangeapi.configuration.feign;

import com.ozan.exchangeapi.exception.RatesApiException;
import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Objects;

@Slf4j
public class RatesApiClientErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {
        String errorMessage = StringUtils.EMPTY;
        try {
            if (Objects.nonNull(response.body())) {
                errorMessage = Util.toString(response.body().asReader());
            }
        } catch (IOException e) {
            log.error("Error occurred when decode response body", e);
        }
        throw new RatesApiException(errorMessage);
    }
}
