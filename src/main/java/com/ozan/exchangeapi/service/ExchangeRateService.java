package com.ozan.exchangeapi.service;

import com.ozan.exchangeapi.converter.ExchangeRateConverter;
import com.ozan.exchangeapi.exception.ExchangeRateException;
import com.ozan.exchangeapi.model.enums.Symbol;
import com.ozan.exchangeapi.model.response.ExchangeRateResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ExchangeRateService {

    private final RatesApiService ratesApiService;
    private final ExchangeRateConverter exchangeRateConverter;

    public Double getExchangeRate(Symbol symbol, Symbol base) {
        ExchangeRateResponse exchangeRateResponse = ratesApiService.getExchangeRate(symbol, base);
        return exchangeRateConverter.apply(exchangeRateResponse, symbol)
                .orElseThrow(ExchangeRateException::new);
    }
}
