package com.ozan.exchangeapi.service;

import com.ozan.exchangeapi.converter.ExchangeConversionConverter;
import com.ozan.exchangeapi.converter.ExchangeRateConverter;
import com.ozan.exchangeapi.domain.ExchangeConversion;
import com.ozan.exchangeapi.exception.ConversionException;
import com.ozan.exchangeapi.model.request.ExchangeConversionFilterRequest;
import com.ozan.exchangeapi.model.request.ExchangeConversionRequest;
import com.ozan.exchangeapi.model.response.ExchangeConversionResponse;
import com.ozan.exchangeapi.model.response.ExchangeRateResponse;
import com.ozan.exchangeapi.model.response.PageableResult;
import com.ozan.exchangeapi.repository.ExchangeConversionRepository;
import com.ozan.exchangeapi.repository.specification.ExchangeConversionSpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ExchangeConversionService {

    private final RatesApiService ratesApiService;
    private final ExchangeRateConverter exchangeRateConverter;
    private final ExchangeConversionConverter exchangeConversionConverter;
    private final ExchangeConversionRepository exchangeConversionRepository;

    public ExchangeConversionResponse convert(ExchangeConversionRequest exchangeConversionRequest) {
        ExchangeRateResponse exchangeRateResponse = ratesApiService.getExchangeRate(
                exchangeConversionRequest.getTargetCurrency(),
                exchangeConversionRequest.getSourceCurrency());
        Optional<Double> optionalExchangeRate = exchangeRateConverter.apply(exchangeRateResponse, exchangeConversionRequest.getTargetCurrency());
        if (optionalExchangeRate.isEmpty()) {
            throw new ConversionException();
        }
        ExchangeConversion exchangeConversion = exchangeConversionConverter.apply(
                exchangeConversionRequest.getSourceAmount(),
                optionalExchangeRate.get(),
                exchangeConversionRequest.getTargetCurrency(),
                exchangeConversionRequest.getSourceCurrency());
        ExchangeConversion savedExchangeConversion = exchangeConversionRepository.save(exchangeConversion);
        return exchangeConversionConverter.apply(savedExchangeConversion);
    }

    public PageableResult<ExchangeConversionResponse> getExchangeConversionFilter(ExchangeConversionFilterRequest exchangeConversionFilterRequest) {
        Page<ExchangeConversion> exchangeConversions = exchangeConversionRepository.findAll(
                ExchangeConversionSpecification.getFilterQuery(exchangeConversionFilterRequest),
                PageRequest.of((exchangeConversionFilterRequest.getPage() - 1),
                        exchangeConversionFilterRequest.getSize(),
                        Sort.by(Sort.Direction.DESC, "createdDate"))
        );
        return exchangeConversionConverter.applyPage(exchangeConversions);
    }
}
