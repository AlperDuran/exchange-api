package com.ozan.exchangeapi.service;

import com.ozan.exchangeapi.client.RatesApiClient;
import com.ozan.exchangeapi.model.enums.Symbol;
import com.ozan.exchangeapi.model.response.ExchangeRateResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class RatesApiService {

    private static final long MINUTE = 60000;

    private final RatesApiClient ratesApiClient;

    @Cacheable("exchanges")
    public ExchangeRateResponse getExchangeRate(Symbol target, Symbol source) {
        return ratesApiClient.getExchangeRate(target, source);
    }

    @Scheduled(fixedRate = MINUTE)
    @CacheEvict(value = "exchanges", allEntries = true)
    public void evictExchangeCacheValues() {
        log.info("Evict exchanges cache values");
    }
}
