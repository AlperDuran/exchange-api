package com.ozan.exchangeapi.repository.specification;

import com.ozan.exchangeapi.domain.ExchangeConversion;
import com.ozan.exchangeapi.model.request.ExchangeConversionFilterRequest;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.Objects;

public class ExchangeConversionSpecification {

    private static final String TRANSACTION_ID = "id";
    private static final String DATE = "createdDate";

    private ExchangeConversionSpecification() {
    }

    public static Specification<ExchangeConversion> getFilterQuery(ExchangeConversionFilterRequest contractFilterRequest) {
        return (root, query, cb) -> {
            Predicate conjunction = cb.conjunction();

            if (Objects.nonNull(contractFilterRequest.getTransactionId())) {
                conjunction = cb.and(conjunction, cb.and(cb.equal(root.get(TRANSACTION_ID), contractFilterRequest.getTransactionId())));
            }
            if (Objects.nonNull(contractFilterRequest.getMinDate())) {
                conjunction = cb.and(conjunction, cb.and(cb.greaterThanOrEqualTo(root.get(DATE), contractFilterRequest.getMinDate())));
            }
            if (Objects.nonNull(contractFilterRequest.getMaxDate())) {
                conjunction = cb.and(conjunction, cb.and(cb.lessThanOrEqualTo(root.get(DATE), contractFilterRequest.getMaxDate())));
            }
            query.distinct(true);
            return conjunction;
        };
    }
}
