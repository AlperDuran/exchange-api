package com.ozan.exchangeapi.repository;

import com.ozan.exchangeapi.domain.ExchangeConversion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ExchangeConversionRepository extends JpaRepository<ExchangeConversion, Integer>, JpaSpecificationExecutor<ExchangeConversion> {
}
