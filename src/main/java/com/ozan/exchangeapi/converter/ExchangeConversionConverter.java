package com.ozan.exchangeapi.converter;

import com.ozan.exchangeapi.domain.ExchangeConversion;
import com.ozan.exchangeapi.model.enums.Symbol;
import com.ozan.exchangeapi.model.response.ExchangeConversionResponse;
import com.ozan.exchangeapi.model.response.PageableResult;
import com.ozan.exchangeapi.utils.Clock;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class ExchangeConversionConverter implements Function<ExchangeConversion, ExchangeConversionResponse> {
    public ExchangeConversion apply(Double sourceAmount, Double exchangeRate, Symbol targetCurrency, Symbol sourceCurrency) {
        return ExchangeConversion.builder()
                .targetAmount(sourceAmount * exchangeRate)
                .targetCurrency(targetCurrency.name())
                .sourceAmount(sourceAmount)
                .sourceCurrency(sourceCurrency.name())
                .exchangeRate(exchangeRate)
                .updatedDate(Clock.now())
                .createdDate(Clock.now())
                .build();
    }

    public ExchangeConversionResponse apply(ExchangeConversion conversion) {
        return ExchangeConversionResponse.builder()
                .amount(conversion.getTargetAmount())
                .currency(Symbol.valueOf(conversion.getTargetCurrency()))
                .transactionId(conversion.getId())
                .build();
    }

    public PageableResult<ExchangeConversionResponse> applyPage(Page<ExchangeConversion> conversions) {
        PageableResult<ExchangeConversionResponse> conversionResponsePageableResult = new PageableResult<>();
        conversionResponsePageableResult.setContent(applyList(conversions.getContent()));
        conversionResponsePageableResult.setPage(conversions.getPageable().getPageNumber());
        conversionResponsePageableResult.setSize(conversions.getSize());
        conversionResponsePageableResult.setTotalElements(conversions.getTotalElements());
        conversionResponsePageableResult.setTotalPages(conversions.getTotalPages());
        return conversionResponsePageableResult;
    }

    public List<ExchangeConversionResponse> applyList(List<ExchangeConversion> contract) {
        return contract.stream().map(this).collect(Collectors.toList());
    }
}
