package com.ozan.exchangeapi.converter;

import com.ozan.exchangeapi.model.enums.Symbol;
import com.ozan.exchangeapi.model.response.ExchangeRateResponse;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ExchangeRateConverter {

    public Optional<Double> apply(ExchangeRateResponse exchangeRateResponse, Symbol symbol) {
        if (exchangeRateResponse.getRates().containsKey(symbol)) {
            return Optional.of(exchangeRateResponse.getRates().get(symbol));
        }
        return Optional.empty();
    }
}
