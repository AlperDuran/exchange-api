package com.ozan.exchangeapi.model.enums;

public enum Symbol {

    USD,
    GBP,
    ERROR,
    TRY
}
