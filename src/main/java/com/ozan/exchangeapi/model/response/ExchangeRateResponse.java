package com.ozan.exchangeapi.model.response;

import com.ozan.exchangeapi.model.enums.Symbol;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExchangeRateResponse {

    private Symbol base;
    private Map<Symbol, Double> rates;
    private String date;
}
