package com.ozan.exchangeapi.model.response;

import com.ozan.exchangeapi.model.enums.Symbol;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExchangeConversionResponse {

    private Double amount;
    private Symbol currency;
    private Integer transactionId;
}
