package com.ozan.exchangeapi.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExchangeConversionFilterRequest extends AbstractPagingRequest {

    private LocalDateTime maxDate;
    private LocalDateTime minDate;
    private Integer transactionId;
}
