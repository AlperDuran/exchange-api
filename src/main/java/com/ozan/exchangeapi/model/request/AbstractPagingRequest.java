package com.ozan.exchangeapi.model.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractPagingRequest {
    private Integer page = 1;
    private Integer size = 10;
}
