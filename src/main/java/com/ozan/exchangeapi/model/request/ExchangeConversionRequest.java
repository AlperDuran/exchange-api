package com.ozan.exchangeapi.model.request;

import com.ozan.exchangeapi.model.enums.Symbol;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExchangeConversionRequest {

    private Double sourceAmount;
    private Symbol sourceCurrency;
    private Symbol targetCurrency;
}
