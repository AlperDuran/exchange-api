package com.ozan.exchangeapi.exception;

public class RatesApiException extends RuntimeException {

    public RatesApiException(String errorMessage) {
        super(errorMessage);
    }
}
